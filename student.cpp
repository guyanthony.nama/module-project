#include "student.h"

/**
* Creation etudiant
*/
Student::Student(std::string name, bool present): m_name(name), m_present(present)
{

}

/**
* Nom etudiant
*/
std::string Student::name() const
{
    return m_name;
}

/**
* Renomer etudiant
*/
void Student::setName(const std::string &name)
{
    m_name = name;
}

/**
* Presence etudiant.
* true si oui, false sinon
*/
bool Student::present() const
{
    return m_present;
}

void Student::setPresent(bool present)
{
    m_present = present;
}

std::string Student::print() const
{
    std::string out = name() + " " + (present()?"true":"false");
    return out;
}

int Student::nb_presents(const std::vector<Student> &students)
{
    int n = 0;
    for(int i=0;i<students.size();i++)
        if(students.at(i).present())
            n++;
    return n;
}
